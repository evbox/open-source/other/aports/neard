# Contributor: Minecrell <minecrell@minecrell.net>
# Maintainer: Minecrell <minecrell@minecrell.net>
pkgname=neard
pkgver=0.16
pkgrel=5
pkgdesc="Near Field Communication manager"
url="https://01.org/linux-nfc/"
arch="all"
license="GPL-2.0-only"
depends="dbus"
makedepends="glib-dev dbus-dev libnl-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="
	0001-Expose-uid-tag-property-to-dbus.patch
	0001-Add-introspection-directory-with-dbus-.xml-files.patch
	0001-neard-Improve-debugability.patch
	0002-nfctools-Add-NFC-ID-after-tag-detection.patch
	https://cdn.kernel.org/pub/linux/network/nfc/$pkgname-$pkgver.tar.xz
	"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--disable-systemd \
		--enable-tools
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install

	# Avoid conflicts with other packages
	mv "$pkgdir"/usr/include/version.h "$pkgdir"/usr/include/near/version.h

	_interfacesdir=$(pkg-config --variable="interfaces_dir" "dbus-1")
	mkdir -p "${pkgdir}/${_interfacesdir}"
	cp -R introspection/* "${pkgdir}/${_interfacesdir}"
}

sha512sums="d0fd5dcfd19db64e65e4149c76979bc7a438240aa6840b35059fde41a8a84a2424274577de964a925e76cd013d21796f69f5361012b6ac79c546d0abdcfed2a6  neard-0.16.tar.xz"
